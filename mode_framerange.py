import bpy
import blf
import gpu
from gpu_extras.batch import batch_for_shader

from .utils import *
from .types import interpolation_modes, easing_types


def draw_time_framerange(context):
    scene = context.scene
    area = context.area
    sfr = scene.brd_frame_range

    if sfr.range.start == sfr.range.end:
        return

    handle_pixels = []
    for h in sfr.range.handles:
        (x, _) = context.region.view2d.view_to_region(h.frame, 0, clip=False)
        handle_pixels.append(x)

    col = (0.33, 0.75, 0.33, 0.33)
    col2 = (0.33, 0.75, 0.33, 0.15)
    handle_col = (0, 0, 0, 0.75)
    handle_sel_col = (1.0, 0.521569, 0.0, 1.0)
    gradient_sel_col = (1.0, 0.521569, 0.0, 0.33)
    gradient_sel_col2 = (1.0, 0.521569, 0.0, 0.15)

    #Hard-coded size of the graph editors vertical bars' positions
    bottom = 0
    top = area.height-49

    #Draw the gradient box from handle 1 to 2, and the black line for 1
    if sfr.range.handles[0].frame < sfr.range.handles[1].frame:
        draw_gradient_box_from_points(
            handle_pixels[0], bottom, 
            handle_pixels[1], top, 
            gradient_sel_col if sfr.range.handles[1].select else col,
            gradient_sel_col2 if sfr.range.handles[0].select else col2)
        # draw_box_from_points(handle_pixels[0], bottom, handle_pixels[0]+1, top, handle_col)
    #Draw solid box from handle 2 to 3
    draw_gradient_box_from_points(
        handle_pixels[1], bottom, 
        handle_pixels[2], top, 
        gradient_sel_col if sfr.range.handles[2].select else col, 
        gradient_sel_col if sfr.range.handles[1].select else col)
    #Same as above but for 3 to 4, line on 4
    if sfr.range.handles[3].frame > sfr.range.handles[2].frame:
        draw_gradient_box_from_points(
            handle_pixels[2], bottom, 
            handle_pixels[3], top, 
            gradient_sel_col2 if sfr.range.handles[3].select else col2, 
            gradient_sel_col if sfr.range.handles[2].select else col)

    # Draw the handles
    for i in range(0, len(sfr.range.handles)):
        handle = sfr.range.handles[i]
        x = handle_pixels[i]

        #Skip drawing left falloff handle if not less than frame start handle
        if i == 0:
            if not (sfr.range.handles[0].frame < sfr.range.handles[1].frame):
                if handle.select and context.scene.anim_mode == 'FRAMERANGE' and get_preferences().show_selected_handles_frame:
                    alphad = (handle_sel_col[0],handle_sel_col[1],handle_sel_col[2],0)
                    draw_gradient_box_from_points(
                        handle_pixels[0], bottom, 
                        handle_pixels[1]-25, top, 
                        alphad, handle_sel_col)
                continue
        
        #Skip drawing right falloff handle if not greater than frame end handle
        if i == 3:
            if not (sfr.range.handles[3].frame > sfr.range.handles[2].frame):
                if handle.select and context.scene.anim_mode == 'FRAMERANGE' and get_preferences().show_selected_handles_frame:
                    alphad = (handle_sel_col[0],handle_sel_col[1],handle_sel_col[2],0)
                    draw_gradient_box_from_points(
                        handle_pixels[2]+1, bottom, 
                        handle_pixels[3]+25, top, 
                        alphad, handle_sel_col)
                continue

        #Draw handle with selection color or not
        if handle.select:
            if context.scene.anim_mode == 'FRAMERANGE' and get_preferences().show_selected_handles_frame:
                draw_handle_selection(handle.frame, handle_sel_col, context)
            else:
                draw_box_from_points(x, bottom, x+1, top, handle_sel_col)
        else:
            draw_box_from_points(x, bottom, x+1, top, handle_col)

    # Falloff guide
    shader = None
    
    if get_preferences().use_beauty_drawing:
        shader = gpu.shader.from_builtin('POLYLINE_SMOOTH_COLOR')
        shader.uniform_float("viewportSize", gpu.state.viewport_get()[2:])
        shader.uniform_float("lineWidth", get_preferences().influence_line_width * bpy.context.preferences.system.pixel_size)
    else:
        shader = gpu.shader.from_builtin('SMOOTH_COLOR')
        gpu.state.line_width_set(get_preferences().influence_line_width * bpy.context.preferences.system.pixel_size)
    
    gpu.state.blend_set("ALPHA")

    verts = []
    cols = []

    scrub_bottom = area.height-47
    scrub_top = area.height-28

    if sfr.range.handles[0].frame == sfr.range.handles[1].frame:
        verts.append((handle_pixels[0], scrub_bottom))
        cols.append((0.5, 1, 0.5, 0.15))

    for frame in range(sfr.range.start, sfr.range.end+1):
        if sfr.range.start == frame and sfr.range.handles[0].frame == sfr.range.handles[1].frame:
            verts.append((handle_pixels[0], scrub_top))
            cols.append((0.5, 1, 0.5, 0.33333))
            continue
        if sfr.range.end == frame and sfr.range.handles[2].frame == sfr.range.handles[3].frame:
            verts.append((handle_pixels[3], scrub_top))
            cols.append((0.5, 1, 0.5, 0.33333))
            continue
        
        #No points need to be computed between the influence range that is always 1,
        # it will stretch
        if sfr.range.handles[1].frame < frame < sfr.range.handles[2].frame:
            continue
        # y gets influenced by the value of easing, but not x
        frm_infl = sfr.get_frame_influence(frame)
        y = scale(frm_infl, scrub_bottom, scrub_top)
        #Stretch normalized value to falloff and frame's start and end 
        # position within the viewport
        infl = normalize(frame, sfr.range.start, sfr.range.end)
        x = scale(infl, handle_pixels[0], handle_pixels[3])
        verts.append((x, y))
        cols.append((0.5, 1, 0.5, max(0.15, frm_infl/3)))

    if sfr.range.handles[2].frame == sfr.range.handles[3].frame:
        verts.append((handle_pixels[3], scrub_bottom))
        cols.append((0.5, 1, 0.5, 0.15))
    
    batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": verts, "color": cols})

    batch.draw(shader)
    
    # Defaults
    gpu.state.line_width_set(1.0)
    gpu.state.blend_set('NONE')