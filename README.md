# Procedural Fcurve Editor

An add-on for blender that adds a few tools to make work with areas of animation curves a little easier.

[Download](https://gitlab.com/bird_d/procedural_fcurve_editor/-/releases)

[Wiki](https://gitlab.com/bird_d/procedural_fcurve_editor/-/wikis/home)

# Todo
- [x] Restructure code in to different files for readability.
- [x] Add shortcuts to keymap on add-on load.
- [x] Add a function to map the current time selection to a modifier.
- [ ] Add videos that show off what this does.
- [ ] Add better comments after splitting files.
- [ ] Add a more interactive gui, not constrained by having to press all sorts of buttons to activate operators.

# Fcurve filter operators
- [x] Cursor
- [x] Playhead
- [x] Zeroing
- [x] Noise
- [x] Weighted Moving Average Smooth
- [ ] Butterworth Smoothing
- [x] Ramp
- [x] Floor
- [x] Ceiling