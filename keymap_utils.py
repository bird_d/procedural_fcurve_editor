import bpy

global_addon_keymaps = []
toggle_addon_keymaps = []
keymap_active_storage = []

keymap_whitelist = [
    "anim.change_frame",
    "graph.cursor_set",
    "wm.context_toggle",
]

def disable_blender_keymaps():
    if len(keymap_active_storage) > 0:
        return

    keymap_list = [
        'Graph Editor',
        'Graph Editor Generic',
        'Dopesheet',
        'Dopesheet Generic',
        'NLA Editor',
        'NLA Channels',
        'NLA Generic',
    ]

    for key in keymap_list:
        keymap_items = []
        km = bpy.context.window_manager.keyconfigs.active.keymaps[key]

        
        for kmi in km.keymap_items:
            if kmi.idname in keymap_whitelist:
                continue

            was_active = kmi.active
            kmi.idname
            keymap_items.append((kmi, was_active))
            kmi.active = False
        
        keymap_active_storage.append(keymap_items)
    
    # Enable brd stuff
    for km, kmi in toggle_addon_keymaps:
        kmi.active = True


def enable_blender_keymaps():

    for item in keymap_active_storage:
        for kmi, was_active in item:
            kmi.active = was_active

    keymap_active_storage.clear()

    # Disable brd stuff
    for km, kmi in toggle_addon_keymaps:
        kmi.active = False


def create_keymaps_from_items(keymap, items):
    for idname, params, options in items:
            kmi = keymap.keymap_items.new(idname, type=params['type'], value=params['value'])
            
            for key in ['ctrl', 'shift', 'alt']:
                # print(key)
                # print(key, params.has_key(key))
                if key in params:
                    # print(key, params[key]))
                    setattr(kmi, key, params[key])


            if options:
                if "properties" in options:
                    for key, value in options.get("properties"):
                        print(key, value)
                        setattr(kmi.properties, key, value)
            
            kmi.active = False
            toggle_addon_keymaps.append((keymap, kmi))


def register_brd_keymap():
    ## Add shortcuts
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc:
        # Keymaps needed by all editors dealing with animation
        km = kc.keymaps.new(name='Animation', space_type='EMPTY')
        ## Change Anim modes ##
        # Keyframe
        kmi = km.keymap_items.new("anim.brd_change_mode", type='ONE', value='PRESS')
        kmi.properties.type = "KEYFRAME"
        global_addon_keymaps.append((km, kmi))
        # Frame Range
        kmi = km.keymap_items.new("anim.brd_change_mode", type='TWO', value='PRESS')
        kmi.properties.type = "FRAMERANGE"
        global_addon_keymaps.append((km, kmi))
        # Modifier Ranges
        # kmi = km.keymap_items.new("anim.brd_change_mode", type='THREE', value='PRESS')
        # kmi.properties.type = "MODIFIERRANGE"
        # global_addon_keymaps.append((km, kmi))

        items = [
            ("brd_framerange.set_range", {"type": 'LEFTMOUSE', "value": 'CLICK_DRAG', "shift": True}, None),
            ("brd_framerange.set_preview_range", {"type": 'LEFTMOUSE', "value": 'CLICK_DRAG', "ctrl": True}, None),
            ("brd_framerange.timerange_clear", {"type": 'X', "value": 'PRESS'}, None),
            ("brd_framerange.select_all_handles", {"type": 'A', "value": 'PRESS'},
                {"properties": [("action", 'SELECT')]}),
            ("brd_framerange.select_all_handles", {"type": 'A', "value": 'PRESS', "alt": True},
                {"properties": [("action", 'DESELECT')]}),
            ("brd_framerange.select_all_handles", {"type": 'I', "value": 'PRESS', "ctrl": True},
                {"properties": [("action", 'INVERT')]}),
            ("brd_framerange.select_framerange_handle", {"type": 'LEFTMOUSE', "value": 'PRESS'},
                {"properties": [("action", 'SET')]}),
            ("brd_framerange.select_framerange_handle", {"type": 'LEFTMOUSE', "value": 'CLICK', "shift": True},
                {"properties": [("action", 'EXTEND')]}),
            ("brd_framerange.grab_timerange_handles", {"type": 'G', "value": 'PRESS'}, None),
            ("brd_framerange.grab_timerange_handles", {"type": 'LEFTMOUSE', "value": "CLICK_DRAG"}, None),
            ("brd_framerange.grow_falloff_handles", {"type": 'S', "value": 'PRESS', "alt": True},
                {"properties": [("action", 'SCALE')]}),
            ("brd_framerange.grow_falloff_handles", {"type": 'WHEELUPMOUSE', "value": 'PRESS', "shift": True, "alt": True},
                {"properties": [("action", 'SCROLL'), ("direction", 1)]}),
            ("brd_framerange.grow_falloff_handles", {"type": 'WHEELDOWNMOUSE', "value": 'PRESS', "shift": True, "alt": True},
                {"properties": [("action", 'SCROLL'), ("direction", -1)]}),
            ("brd_framerange.scale_timerange_handles", {"type": 'S', "value": 'PRESS'}, None),
            ("wm.call_menu_pie", {"type": 'G', "value": 'PRESS', "shift": True},
                {"properties": [("name", 'BRD_FRAMERANGE_MT_PIE_snap_transforms')]}),
        ]

        create_keymaps_from_items(km, items)

        # Graph Specific keymaps
        km = kc.keymaps.new(name='Graph Editor Generic', space_type='GRAPH_EDITOR')

        items = [
            ("wm.call_menu_pie", {"type": 'F', "value": 'PRESS'},
                {"properties": [("name", 'GRAPH_MT_PIE_pfc_procedural_filters')]}),
        ]

        create_keymaps_from_items(km, items)

        

        # kmi = km.keymap_items.new("wm.call_menu", type='F', value='PRESS', shift=True)
        # kmi.properties.name = "GRAPH_MT_procedural_fcurves"
        # toggle_addon_keymaps.append((km, kmi))


def unregister_brd_keymap():
    enable_blender_keymaps()

    for km, kmi in toggle_addon_keymaps:
        km.keymap_items.remove(kmi)
    toggle_addon_keymaps.clear()

    for km, kmi in global_addon_keymaps:
        km.keymap_items.remove(kmi)
    global_addon_keymaps.clear()
