    # Procedural Fcurve Editor, Adds tools to make modifying fcurves easier.
    # Copyright (C) 2020 bird_d <relay198@gmail.com>

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <https://www.gnu.org/licenses/>.

bl_info = {
    "name": "Procedural Fcurves",
    "author": "bird_d",
    "version": (0, 1, 5),
    "blender": (2, 83, 0),
    "location": "Graph Editor > F3 > 'Procedural'",
    "description": "Some stuff for procedural modification of FCurves",
    "warning": "",
    "doc_url": "https://gitlab.com/bird_d/procedural-fcurve-editor/-/wikis/home",
    "category": "Animation",
}

# Reload modules if we're reloading the add-on
if "bpy" in locals():
    import importlib
    importlib.reload(operators)
    importlib.reload(ui)
    importlib.reload(keymap_utils)
    importlib.reload(utils)
    importlib.reload(mode_framerange)
else:
    from . import operators
    from . import ui
    from . import keymap_utils
    from .utils import *
    from .mode_framerange import *
    from .types import *

import bpy
import gpu
import random
from gpu_extras.batch import batch_for_shader
import blf
from bpy.types import AddonPreferences
from bpy.props import IntProperty, FloatProperty, BoolProperty, EnumProperty, StringProperty
from bpy.app.handlers import persistent

# Used to kill the global modal when the addon is disabled
global_modal_active = False
graph_draw_handle = None
dopesheet_draw_handle = None

class ProceduralFcurve_Settings(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__

    # TODO: Rename to sync_preview_time_range, and move to scene data
    sync_preview_range : BoolProperty(
        default=False, 
        description="Automatically set the preview range when the procedural fcurve range updates")

    deselect_new_keyframes : BoolProperty(
        default=True, 
        description="Deselects the keyframe handles during an fcurve operators' modal")

    show_hover_handle_influence : BoolProperty(
        default=True,
        description="Show the influence under the green handle"
    )

    show_selected_handles_frame : BoolProperty(
        default=True,
        description="Show the frame on selected handles, similar to how the playhead is drawn"
    )

    use_beauty_drawing : BoolProperty(
        default=True,
        description="Draw the influence line with anti-aliasing"
    )

    show_hover_handle : BoolProperty(
        default=True,
        description="Show the handle that appears when holding shift"
    )

    influence_line_width : FloatProperty(
        default=1.5,
        description="Width of the influence line of the scene time range, drawn in the scrubber area",
        min=0.01,
        max=10.0
    )

    use_mode_backdrops : BoolProperty(
        default=True,
        description="Show a light overlay in the background to differentiate different modes")

    def draw(self, context):
        layout = self.layout
        box = layout.box()
        box.prop(self, "deselect_new_keyframes", text="Deselect keyframes during modal")
        box.prop(self, "use_mode_backdrops", text="Add backdrops for each mode")
        box.prop(self, "show_hover_handle", text="Show Frame Hover Scrubber")
        box.prop(self, "show_hover_handle_influence", text="Show the influence value on the frame hover scrubber")
        box.prop(self, "show_selected_handles_frame", text="Show Selected Handle(s) Frame")
        box.prop(self, "influence_line_width", text="Influence Line Width")
        box.prop(self, "use_beauty_drawing", text="Use Beauty Drawing")

def global_draw():
    context = bpy.context
    area = context.area
    area.tag_redraw()
    preferences = get_preferences()

    #Draw a slight color on the background if we're in edit_mode
    if preferences.use_mode_backdrops:
        backdrop_col = { 
            "KEYFRAME": (0.95, 0.95, 0.33, 0.05), 
            "FRAMERANGE": (0.33, 0.95, 0.33, 0.05),
            "MODIFIERRANGE": (0.33, 0.33, 0.95, 0.05)
        }

        draw_box_from_points(
            0, 0, 
            area.width, area.height-49, 
            backdrop_col[context.scene.anim_mode]
        )

    draw_time_framerange(context)

class ANIM_OT_brd_global_modal(bpy.types.Operator):
    """Change mode TODO THIS DIALOGUE"""
    bl_idname = "anim.brd_global_modal"
    bl_label = "Modal for handling framerange modes, runs one time when add-on is enabled"

    mouse_x_start: IntProperty()
    hold_handle: IntProperty()
    pressing: BoolProperty(default=False)
    left_mouse_pressed = False
    shift_pressed = False
    hover_frame = 0
    click_frame = 0
    selection_handle_type = 'TIMERANGE'
    dragging_preview = False

    def modal(self, context, event):
        global global_modal_active
        scene = context.scene

        if global_modal_active == False and event.value == 'RELEASE':
            return {'FINISHED'}

        if not global_modal_active or scene.anim_mode == "KEYFRAME":
            global_modal_active = False
            return {'PASS_THROUGH'}

        area = None
        region = None

        for a in context.screen.areas:
            if a.type not in {'GRAPH_EDITOR', 'DOPESHEET_EDITOR'}:
                continue
            for r in a.regions:
                if r.type == 'WINDOW':
                    if (event.mouse_x >= r.x and
                        event.mouse_y >= r.y and
                        event.mouse_x < r.width + r.x and
                        event.mouse_y < r.height + r.y
                    ):
                        area = a
                        region = r
                        break

        if area == None:
            return {'PASS_THROUGH'}

        return {'PASS_THROUGH'}


    def invoke(self, context, event):
        # Make sure we initialize the handles or bad things happen
        while len(context.scene.brd_frame_range.range.handles) < 4:
            context.scene.brd_frame_range.range.handles.add()

        context.window_manager.modal_handler_add(self)

        return {'RUNNING_MODAL'}


class ANIM_OT_brd_change_mode(bpy.types.Operator):
    """Change mode TODO THIS DIALOGUE"""
    bl_idname = "anim.brd_change_mode"
    bl_label = "Change the animation mode"

    type : bpy.props.EnumProperty(items=[
        ("KEYFRAME", "Keyframe", "", 1),
        ("FRAMERANGE", "FrameRange", "", 2),
        # ("MODIFIERRANGE", "ModifierRange", "", 3),
    ])

    def invoke(self, context, event):
        context.scene.anim_mode = self.type
        context.area.tag_redraw()
        return {'FINISHED'}


# Anim mode #
anim_mode_enum = [
    ("KEYFRAME", "Keyframe", "", "KEYFRAME", 1),
    ("FRAMERANGE", "FrameRange", "", "MOD_TIME", 2),
    # ("MODIFIERRANGE", "ModifierRange", "", "MODIFIER_DATA", 3),
]

anim_mode = 1

def get_enum(self):
    return anim_mode

def set_enum(self, value):
    global anim_mode

    if anim_mode == value:
        return

    if value == 1: # Keyframe mode
        keymap_utils.enable_blender_keymaps()
    elif value == 2: # Framerange mode
        keymap_utils.disable_blender_keymaps()

    anim_mode = value

def update_enum(self, value):
    global global_modal_active
    if self.anim_mode == "KEYFRAME" or global_modal_active:
        return
    global_modal_active = True
    bpy.ops.ANIM.brd_global_modal('INVOKE_DEFAULT')

#region    #Registration#
classes = [
    ProceduralFcurve_Settings,
    BRD_FrameRangeHandle,
    BRD_FrameRange,
    BRD_FrameRange_Selection,
    # Modal operator to handle input for each mode
    ANIM_OT_brd_global_modal,
    ANIM_OT_brd_change_mode,
    # operators/ranges.py
    operators.ranges.BRD_FRAMERANGE_OT_set_range,
    operators.ranges.BRD_FRAMERANGE_OT_set_preview_range,
    operators.ranges.BRD_FRAMERANGE_OT_timerange_clear,
    operators.ranges.BRD_FRAMERANGE_OT_grab_timerange_handles,
    operators.ranges.BRD_FRAMERANGE_OT_select_framerange_handle,
    operators.ranges.BRD_FRAMERANGE_OT_select_all_handles,
    operators.ranges.BRD_FRAMERANGE_OT_grow_falloff_handles,
    operators.ranges.BRD_FRAMERANGE_OT_snap_timerange_handles,
    operators.ranges.BRD_FRAMERANGE_OT_scale_timerange_handles,
    # operators/fcurves.py
    operators.fcurves.GRAPH_OT_pfc_filter_noise,
    operators.fcurves.GRAPH_OT_pfc_filter_wma_smooth,
    operators.fcurves.GRAPH_OT_pfc_filter_ramp,
    operators.fcurves.GRAPH_OT_pfc_filter_cursor,
    operators.fcurves.GRAPH_OT_pfc_filter_playhead,
    operators.fcurves.GRAPH_OT_pfc_filter_zero,
    operators.fcurves.GRAPH_OT_pfc_filter_limit,
    operators.fcurves.GRAPH_OT_procedural_range_to_modifier,
    # operators.fcurves.FRAMERANGE_OT_decimate,
    # ui/fcurves.py
    ui.fcurves.GRAPH_MT_procedural_fcurves,
    ui.fcurves.GRAPH_MT_PIE_procedural_transform,
    ui.fcurves.BRD_FRAMERANGE_MT_PIE_snap_transforms,
    ui.fcurves.GRAPH_MT_PIE_pfc_procedural_filters
]


# load_post hook
@persistent
def load_post_enable_anim_modal(dummy):
    global global_modal_active
    global_modal_active = False

    global anim_mode
    anim_mode = 1

    keymap_utils.enable_blender_keymaps()


def register():
    # Register classes
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.brd_frame_range = bpy.props.PointerProperty(type=BRD_FrameRange_Selection)
    
    bpy.types.Scene.anim_mode = bpy.props.EnumProperty(
            items=anim_mode_enum, get=get_enum, set=set_enum, update=update_enum)

    # Append to graph header
    bpy.types.GRAPH_HT_header.append(ui.fcurves.falloff_type_draw_item)
    bpy.types.DOPESHEET_HT_header.append(ui.fcurves.falloff_type_draw_item)

    # Add draw handles
    global graph_draw_handle, dopesheet_draw_handle
    graph_draw_handle = bpy.types.SpaceGraphEditor.draw_handler_add(
                global_draw, (), 'WINDOW', 'POST_PIXEL')
    dopesheet_draw_handle = bpy.types.SpaceDopeSheetEditor.draw_handler_add(
                global_draw, (), 'WINDOW', 'POST_PIXEL')

    keymap_utils.register_brd_keymap()

    # Add on load file hook
    if not load_post_enable_anim_modal in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(load_post_enable_anim_modal)


def unregister():
    # Remove load file hook
    if load_post_enable_anim_modal in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(load_post_enable_anim_modal)

    #Remove from headers
    bpy.types.GRAPH_HT_header.remove(ui.fcurves.falloff_type_draw_item)
    bpy.types.DOPESHEET_HT_header.remove(ui.fcurves.falloff_type_draw_item)

    # Remove draw handlers
    bpy.types.SpaceGraphEditor.draw_handler_remove(graph_draw_handle, 'WINDOW')
    bpy.types.SpaceDopeSheetEditor.draw_handler_remove(dopesheet_draw_handle, 'WINDOW')

    keymap_utils.unregister_brd_keymap()

    #Unregister classes
    for cls in classes:
        bpy.utils.unregister_class(cls)

#endregion #Registration#

if __name__ == "__main__":
    try:
        unregister()
    except RuntimeError as e:
        print(e)
    except AttributeError as e:
        print(e)
    except:
        import sys
        print("Error:", sys.exc_info()[0])
        pass
    register()