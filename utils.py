# Procedural Fcurve Editor, Adds tools to make modifying fcurves easier.
# Copyright (C) 2020 bird_d <relay198@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import bpy
import blf
import gpu
from gpu_extras.batch import batch_for_shader
import logging
import time
from math import sin, cos, pi, pow, sqrt
log = logging.getLogger("%s" % (__name__))

"""Easing, for use with a value between 0 and 1

Might not be the best implementation of this stuff, assumes you use 0-1 values
Based on Robert Penner's easing functions and easings.net
"""
class Ease():
    #Sineusoidal
    def in_sine(x):
        return 1 - cos((x * pi) / 2)
    def out_sine(x):
        return sin((x * pi) / 2 )
    def in_out_sine(x):
        return -(cos(x * pi) - 1) / 2
    #Quadratic
    def in_quad(x):
        return x * x
    def out_quad(x):
        return 1 - (1 - x) * (1 - x)
    def in_out_quad(x):
        return 2 * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 2) / 2
    #Cubic
    def in_cubic(x):
        return x * x * x
    def out_cubic(x):
        return 1 - pow(1 - x, 3)
    def in_out_cubic(x):
        return 4 * x * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 3) / 2
    #Quartic
    def in_quart(x):
        return x * x * x * x
    def out_quart(x):
        return 1 - pow(1 - x, 4)
    def in_out_quart(x):
        return  8 * x * x * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 4) / 2
    #Quintic
    def in_quint(x):
        return x * x * x * x * x
    def out_quint(x):
        return 1 - pow(1 - x, 5)
    def in_out_quint(x):
        return  16 * x * x * x * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 5) / 2
    #Exponential
    def in_expo(x):
        return 0 if x == 0 else pow(2, 10 * x - 10)
    def out_expo(x):
        return 1 if x == 1 else 1 - pow(2, -10 * x)
    def in_out_expo(x):
        return 0 if x == 0 else 1 if x == 1 else pow(2, 20 * x - 10) / 2 if x < 0.5 else (2 - pow(2, -20 * x + 10)) / 2
    #Circular
    def in_circ(x):
        return 1 - sqrt(1 - pow(x, 2))
    def out_circ(x):
        return sqrt(1 - pow(x - 1, 2))
    def in_out_circ(x):
        return (1 - sqrt(1 - pow(2 * x, 2))) / 2 if x < 0.5 else (sqrt(1 - pow(-2 * x + 2, 2)) + 1) / 2
    #Back
    def in_back(x):
        c1 = 1.70158
        c3 = c1 + 1
        return c3 * x * x * x - c1 * x * x
    def out_back(x):
        c1 = 1.70158
        c3 = c1 + 1
        return 1 + c3 * pow(x - 1, 3) + c1 * pow(x - 1, 2)
    def in_out_back(x):
        c1 = 1.70158
        c2 = c1 * 1.525
        return (pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2 if x < 0.5 else (pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2
    def in_elastic(x):
        c4 = (2 * pi) / 3
        return 0 if x == 0 else 1 if x == 1 else -pow(2, 10 * x - 10) * sin((x * 10 - 10.75) * c4)
    def out_elastic(x):        
        c4 = (2 * pi) / 3
        return 0 if x == 0 else 1 if x == 1 else pow(2, -10 * x) * sin((x * 10 - 0.75) * c4) + 1
    def in_out_elastic(x):
        c5 = (2 * pi) / 4.5
        return 0 if x == 0 else 1 if x == 1 else -(pow(2, 20 * x - 10) * sin((20 * x - 11.125) * c5)) / 2 if x < 0.5 else (pow(2, -20 * x + 10) * sin((20 * x - 11.125) * c5)) / 2 + 1
    def in_bounce(x):
        return 1 - Ease.out_bounce(1 - x)
    def out_bounce(x):
        n1 = 7.5625
        d1 = 2.75

        if (x < 1 / d1):
            return n1 * x * x
        elif (x < 2 / d1):
            tmp = (x - 1.5 / d1)
            return n1 * tmp * tmp + 0.75
        elif (x < 2.5 / d1):
            tmp = (x - 2.25 / d1)
            return n1 * tmp * tmp + 0.9375
        tmp = (x - 2.625 / d1)
        return n1 * tmp * tmp + 0.984375
    def in_out_bounce(x):
        return (1 - Ease.out_bounce(1 - 2 * x)) / 2 if x < 0.5 else (1 + Ease.out_bounce(2 * x - 1)) / 2
#Helpful functions


def clamp(val, lower=0, upper=1):
    return lower if val < lower else upper if val > upper else val


def get_active_fcurves(context):
    start_time = time.time()
    fcurves = []
    active_bone_paths = []
    # Get selected pose bones or use object, since fcurves can be selected even 
    # when they're not displaying in the graph editor
    obj = context.active_object
    if obj and obj.type == 'ARMATURE' and obj.mode == 'POSE':
        for pb in context.selected_pose_bones:
            active_bone_paths.append('pose.bones["%s"]' % pb.name)
    else:
        active_bone_paths += ["location", 
                              "rotation_euler", 
                              "scale", 
                              "rotation_quaternian", 
                              "rotation_axis"]

    # Make sure fcurve is selected, has correct data path, and is not hidden
    for fc in context.active_object.animation_data.action.fcurves:
        if fc.select and fc.data_path.rsplit('.', 1)[0] in active_bone_paths and not fc.hide:
            fcurves.append(fc)
    log.debug("Time to get Fcurves: %.8fs" % (time.time() - start_time))
    log.debug("Fcurves: %d" % len(fcurves))
    return fcurves


def normalize(val, min_val, max_val):
    if min_val == max_val:
        return val
    return ((val - min_val) / (max_val - min_val))


def scale(scale, min_val, max_val):
    return (scale * (max_val - min_val)) + min_val


def get_preferences():
    return bpy.context.preferences.addons[__package__].preferences


## Common fcurve code
def handle_common_errors(self):
    if bpy.context.area.type != 'GRAPH_EDITOR':
        self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
        return False
    bfr = bpy.context.scene.brd_frame_range
    if bfr.range.start == bfr.range.end:
        self.report({'WARNING'}, "No area selected to modify.")
        return False
    if len(self.fcurves) == 0:
        self.report({'WARNING'}, "Must have Fcurve channel(s) selected.")
        return False
    return True


def insert_keyframe_point(fcurve, frame, value, options, keyframe_type='KEYFRAME', deselect=False):
    kp = fcurve.keyframe_points.insert(frame=frame, value=value, options=options, keyframe_type=keyframe_type)

    if deselect:
        kp.select_control_point = False
        kp.select_left_handle = False
        kp.select_right_handle = False
    return kp


## Custom drawing
def draw_box_from_points(x=0, y=0, x2=200, y2=100, col=(1, 0, 0.75, 1.0)):
    shader = gpu.shader.from_builtin('UNIFORM_COLOR')
    shader.uniform_float("color", col)
    
    gpu.state.blend_set('ALPHA')

    verts = [(x,y), (x2,y), (x2,y2), (x,y2)]
    faces = [ verts[0], verts[1], verts[2], verts[3], verts[2], verts[0] ]

    batch = batch_for_shader(shader, 'TRIS', {"pos": faces})
    batch.draw(shader)
    
    #Defaults
    gpu.state.blend_set('NONE')


def draw_line(x1, y1, x2, y2, col, width=1.0):
    shader = gpu.shader.from_builtin('UNIFORM_COLOR')
    shader.uniform_float("color", col)
    
    gpu.state.line_width_set(width)

    # fmt = gpu.types.GPUVertFormat()
    # fmt.attr_add(id = "pos", comp_type = 'F32', len = 2, fetch_mode = 'FLOAT')

    verts = [(x1,y1), (x2,y2)]
    
    batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": verts})
    
    
    batch.draw(shader)
    gpu.state.line_width_set(1.0)


def draw_handle_selection(frame, col, context):
    (x, _) = context.region.view2d.view_to_region(frame, 0, clip=False)
    y = context.area.height-36
    draw_line(x, 0, x, y, col, 2.0)
    width, height = (24, 20)
    draw_box_from_points(x - (width/2), y - (height/2), x + (width/2) + 1, y + (height/2) - 3, col)

    #Text
    font_id = 0

    size, dpi = (28, 28)
    blf.size(0, size, dpi)
    width, height = (blf.dimensions(0, str(frame)))

    font_x = x - (width/2)
    font_y = y - (height/2)
    blf.position(font_id, font_x, font_y-1, 0)
    blf.color(0, 1.0, 1.0, 1.0, 1.0)
    blf.draw(0, str(frame))


def draw_selection_handle(self, context):
    x,_ = context.region.view2d.view_to_region(self.hover_frame, 0, clip=False)
    draw_box_from_points(x-2, 11, x+2, context.area.height-49, (1, 0.75, 0.5, 0.85))


def draw_gradient_box_from_points(x=0, y=0, x2=200, y2=100, col=(1, 0, 0.75, 1.0), col2=(1,0,0,1)):
    shader = gpu.shader.from_builtin('SMOOTH_COLOR')
    
    gpu.state.blend_set('ALPHA')

    verts = [(x,y), (x2,y), (x2,y2), (x,y2)]
    faces = [ verts[0], verts[1], verts[2], verts[0], verts[2], verts[3] ]
    vcols = [col2, col, col, col2, col, col2]

    batch = batch_for_shader(shader, 'TRIS', {"pos": faces, "color": vcols})
    batch.draw(shader)
    
    #Defaults
    gpu.state.blend_set('NONE')