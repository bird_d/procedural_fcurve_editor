from bpy.types import Menu
import logging

class GRAPH_MT_procedural_fcurves(Menu):
    bl_label = ""
    bl_idname = "GRAPH_MT_procedural_fcurves"
    
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        col = row.column()
        col.operator_context = "INVOKE_DEFAULT"

        col.label(text="Fcurve Manipulation")
        col.separator()
        col.operator("graph.pfc_filter_cursor", text="Cursor", icon="CURSOR")
        col.operator("graph.pfc_filter_cursor", text="Playhead", icon="PLAY")
        col.operator("graph.pfc_filter_cursor", text="Zero", icon="IMPORT")
        col.operator("graph.pfc_filter_noise", text="Noise", icon="MOD_NOISE")
        col.operator("graph.pfc_filter_wma_smooth", text="WMA Smooth", icon="MOD_SMOOTH")
        col.operator("graph.pfc_filter_ramp", text="Ramp", icon="IPO_LINEAR")
        col.operator("graph.pfc_filter_limit", text="Floor", icon="EXPORT")['type'] = 'FLOOR'
        col.operator("graph.pfc_filter_limit", text="Ceiling", icon="IMPORT")['type'] = 'CEILING'

        # Modifier Col
        col = row.column()
        col.label(text="Modifier Range")
        col.separator()
        col.operator("graph.procedural_range_to_modifier", text="Active", icon="LAYER_ACTIVE")['modifier'] = 'ACTIVE'
        #col.operator("graph.procedural_range_to_modifier", text="All")
        col.operator("graph.procedural_range_to_modifier", text="Generator", icon="DRIVER_TRANSFORM")['modifier'] = 'GENERATOR'
        col.operator("graph.procedural_range_to_modifier", text="Built-In Function", icon="DRIVER_TRANSFORM")['modifier'] = 'FNGENERATOR'
        col.operator("graph.procedural_range_to_modifier", text="Envelope", icon="CURVE_DATA")['modifier'] = 'ENVELOPE'
        col.operator("graph.procedural_range_to_modifier", text="Cycles", icon="FILE_REFRESH")['modifier'] = 'CYCLES'
        col.operator("graph.procedural_range_to_modifier", text="Noise", icon="MOD_NOISE")['modifier'] = 'NOISE'
        col.operator("graph.procedural_range_to_modifier", text="Limits", icon="CON_DISTLIMIT")['modifier'] = 'LIMITS'
        col.operator("graph.procedural_range_to_modifier", text="Stepped Interpolation", icon="IPO_CONSTANT")['modifier'] = 'STEPPED'

class GRAPH_MT_PIE_procedural_transform(Menu):
    # label is displayed at the center of the pie menu.
    bl_label = "Select Handle"

    def draw(self, context):
        layout = self.layout

        pie = layout.menu_pie()
        # operator_enum will just spread all available options
        # for the type enum of the operator on the pie
        pie.operator_enum("graph.proceduralrange_transform", "handle")

class BRD_FRAMERANGE_MT_PIE_snap_transforms(Menu):
    bl_label = "Snap Time Range Handles"

    def draw(self, context):
        pie = self.layout.menu_pie()

        props = pie.operator("brd_framerange.snap_timerange_handles", text="Left", icon="BACK")
        props.direction = 'LEFT'
        props = pie.operator("brd_framerange.snap_timerange_handles", text="Right", icon="FORWARD")
        props.direction = 'RIGHT'

class GRAPH_MT_PIE_pfc_procedural_filters(Menu):
    bl_label = "Choose filter"

    def draw(self, context):
        pie = self.layout.menu_pie()

        pie.operator("graph.pfc_filter_cursor", text="Cursor", icon="CURSOR")
        pie.operator("graph.pfc_filter_playhead", text="Playhead", icon="PLAY")
        pie.operator("graph.pfc_filter_zero", text="Zero", icon="IMPORT")
        pie.operator("graph.pfc_filter_noise", text="Noise", icon="MOD_NOISE")
        pie.operator("graph.pfc_filter_wma_smooth", text="WMA Smooth", icon="MOD_SMOOTH")
        pie.operator("graph.pfc_filter_ramp", text="Ramp", icon="IPO_LINEAR")
        pie.operator("graph.pfc_filter_limit", text="Floor", icon="EXPORT")['type'] = 'FLOOR'
        pie.operator("graph.pfc_filter_limit", text="Ceiling", icon="IMPORT")['type'] = 'CEILING'

def falloff_type_draw_item(self, context):
    preferences = context.preferences.addons[__package__.split(".")[0]].preferences
    
    layout = self.layout
    row = layout.row(align=True)
    row.prop(context.scene, "anim_mode", expand=True, icon_only=True, icon='STATUSBAR')

    bfr = context.scene.brd_frame_range
    row.prop(bfr, "falloff_left_easing", text="", icon_only=True)
    row.prop(bfr, "falloff_left_interpolation", text="")
    row.prop(bfr, "falloff_right_interpolation", text="")
    row.prop(bfr, "falloff_right_easing", text="", icon_only=True)

    row.prop(preferences, "sync_preview_range", icon_only=True, icon='UV_SYNC_SELECT')