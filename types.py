import bpy
from bpy.props import BoolProperty, CollectionProperty, IntProperty, EnumProperty, PointerProperty
from bpy.types import PropertyGroup
from .utils import *


interpolation_modes = [
    ('LINEAR', 'Linear', '', 'IPO_LINEAR', 0),
    ('SINE', 'Sinusoidal', '', 'IPO_SINE', 1),
    ('QUAD', 'Quadric', '', 'IPO_QUAD', 2),
    ('CUBIC', 'Cubic', '', 'IPO_CUBIC', 3),
    ('QUART', 'Quartic', '', 'IPO_QUART', 4),
    ('QUINT', 'Quintic', '', 'IPO_QUINT', 5),
    ('EXPO', 'Exponential', '', 'IPO_EXPO', 6),
    ('CIRC', 'Circular', '', 'IPO_CIRC', 7),
    ('BACK', 'Back', '', 'IPO_BACK', 8),
    ('ELASTIC', 'Elastic', '', 'IPO_ELASTIC', 9),
    ('BOUNCE', 'Bounce', '', 'IPO_BOUNCE', 10)
]


easing_types = [
    ('IN', 'In', '', 'IPO_EASE_IN', 0),
    ('OUT', 'Out', '', 'IPO_EASE_OUT', 1),
    ('IN_OUT', 'In and Out', '', 'IPO_EASE_IN_OUT', 2)
]


class BRD_FrameRangeHandle(PropertyGroup):
    select  : BoolProperty(default=False)
    frame   : IntProperty(default=0)


class BRD_FrameRange(PropertyGroup):
    handles : CollectionProperty(type=BRD_FrameRangeHandle)
    start   : IntProperty(default=0)
    end     : IntProperty(default=0)

    def update_range(self):
        if self.handles[0].frame > self.handles[1].frame:
            self.handles[0].frame = self.handles[1].frame
        if self.handles[3].frame < self.handles[2].frame:
            self.handles[3].frame = self.handles[2].frame

        hold = self.handles[1].frame
        if self.handles[1].frame > self.handles[2].frame:
            self.handles[1].frame = self.handles[2].frame
        if self.handles[2].frame < hold:
            self.handles[2].frame = hold

        self.start = self.handles[1].frame
        if self.handles[0].frame < self.handles[1].frame:
            self.start = self.handles[0].frame
        self.end = self.handles[2].frame
        if self.handles[3].frame > self.handles[2].frame:
            self.end = self.handles[3].frame


class BRD_FrameRange_Selection(PropertyGroup):
    range : PointerProperty(type=BRD_FrameRange)
    falloff_left_interpolation  : EnumProperty(items=interpolation_modes)
    falloff_left_easing         : EnumProperty(items=easing_types)
    falloff_right_interpolation : EnumProperty(items=interpolation_modes)
    falloff_right_easing        : EnumProperty(items=easing_types)

    def check(self):
        if self.range.handles[1].frame == self.range.handles[2].frame:
            return True
        return None

    # Returns a normalized value based on what frame and where that 
    # lies between the frame handles, and then easing is applied
    def get_frame_influence(self, frame):
        if self.range.start == frame or frame == self.range.end:
            return 0
        if self.range.handles[1].frame <= frame <= self.range.handles[2].frame:
            return 1
        if self.range.handles[0].frame < frame < self.range.handles[1].frame:
            return self._get_falloff_influence(
                frame,
                normalize(frame, self.range.handles[0].frame, self.range.handles[1].frame))
        if self.range.handles[2].frame < frame < self.range.handles[3].frame:
            return self._get_falloff_influence(
                frame,
                normalize(frame, self.range.handles[3].frame, self.range.handles[2].frame))
        return 0

    # Really only should be called by functions within this class
    # It should be used for the normalized value as the easing 
    #   functions only go from 0-1
    def _get_falloff_influence(self, frame, val):
        interpolation_type = 'LINEAR'
        easing_type = 'IN'

        if self.range.handles[0].frame <= frame <= self.range.handles[1].frame:
            interpolation_type = self.falloff_left_interpolation
            easing_type = self.falloff_left_easing

        if self.range.handles[2].frame <= frame <= self.range.handles[3].frame:
            interpolation_type = self.falloff_right_interpolation
            easing_type = self.falloff_right_easing

        if interpolation_type == 'LINEAR':
            return val

        #Use a string to call a function on the utils.py/Ease class we 
        # have imported. Far more maintainable than using an if statement
        return getattr(
            Ease, 
            "%s_%s" % (easing_type.lower(), interpolation_type.lower())) (val)

    def update_range(self):
        self.range.update_range()

        # TODO: Have an option that only does this when a modal that uses time selection
        #     gets activated
        if get_preferences().sync_preview_range:
            scene = bpy.context.scene
            if self.range.start == self.range.end:
                scene.use_preview_range = False
                scene.frame_preview_start = scene.frame_preview_end = 0
            else:
                scene.use_preview_range = True
                scene.frame_preview_start = self.range.start
                scene.frame_preview_end = self.range.end