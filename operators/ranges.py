import bpy
from bpy.types import Operator
from bpy.props import IntProperty, EnumProperty
from ..utils import draw_handle_selection, normalize

# This is just to be lazy for if the name changes since I'm not sure if I want
# it in Scene anymore.
def get_framerange(context):
    return context.scene.brd_frame_range


def tag_redraw_animation_spaces(context):
    for area in context.screen.areas:
        if area.type in {'GRAPH_EDITOR', 'DOPESHEET_EDITOR', 'PLAYBACK', 'NLA_EDITOR'}:
            area.tag_redraw()


# TODO: Make an operator that moves the selected handles to the animation start/end
def draw_set_range_callback(op, context):
    draw_handle_selection(op.hover_frame, (0.368627, 0.701961, 0.407843, 1.0), context)


def set_range_clean(handle, context):
    if context.area.type == 'GRAPH_EDITOR':
        bpy.types.SpaceGraphEditor.draw_handler_remove(handle, 'WINDOW')
    elif context.area.type == 'DOPESHEET_EDITOR':
        bpy.types.SpaceDopeSheetEditor.draw_handler_remove(handle, 'WINDOW')


class BRD_FRAMERANGE_OT_set_range(bpy.types.Operator):
    """Set the scene's frame range"""
    bl_idname = "brd_framerange.set_range"
    bl_label = "Set the scene's frame range"
    bl_options = {'REGISTER', 'UNDO'}
    # log = logging.getLogger("%s.%s" % (__name__, bl_idname))

    mouse_x_start: bpy.props.IntProperty()
    _draw_handle = None

    def modal(self, context, event):
        #Get the right region, else it could run calculations on the wrong region inside the area
        context.area.tag_redraw()
        region = None
        for r in context.area.regions:
            if r.type == "WINDOW":
                region = r
                break
        handles = context.scene.brd_frame_range.range.handles
        if event.type == 'MOUSEMOVE':
            # Make sure lower value between mouse start and mose current x is start
            x1,_ = context.region.view2d.region_to_view(self.mouse_x_start - region.x, 0)
            x2,_ = context.region.view2d.region_to_view(event.mouse_x - region.x, 0)
            start, end = (round(x1), round(x2)) if x1 < x2 else (round(x2), round(x1))
            self.hover_frame = round(x2)

            # 1st behaviour: Set the range without the outer handles
            if self.only_inner:
                handles[0].frame = handles[1].frame = start
                handles[2].frame = handles[3].frame = end
            # 2nd behaviour: Sort values and set every handle using them
            else:
                values = [start, end]
                values.extend(list(set(self.frame_holds)))
                values.sort()

                for i in range(len(handles)):
                    handles[i].frame = values[i]

            context.scene.brd_frame_range.update_range()
            return {'RUNNING_MODAL'}

        if event.type in {'ENTER', 'LEFTMOUSE'}:
            set_range_clean(self._handle, context)
            return {'FINISHED'}

        if event.type in {'ESC', 'RIGHTMOUSE'}:
            for i in range(0, len(handles)):
                handles[i].frame = self.frame_holds[i]
            context.scene.brd_frame_range.update_range()
            set_range_clean(self._handle, context)
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.mouse_x_start = event.mouse_x

        self.frame_holds = []
        self.only_inner = True

        handles = context.scene.brd_frame_range.range.handles

        if handles[1].frame != handles[2].frame \
                and handles[0].frame == handles[1].frame \
                and handles[2].frame == handles[3].frame:
            self.only_inner = False

        for handle in handles:
            self.frame_holds.append(handle.frame)
            handle.select = False

        x,_ = context.region.view2d.region_to_view(event.mouse_x - context.region.x, 0)
        self.hover_frame = round(x)

        if context.area.type == 'GRAPH_EDITOR':
            self._handle = bpy.types.SpaceGraphEditor.draw_handler_add(
                draw_set_range_callback, (self, context), 'WINDOW', 'POST_PIXEL')
        elif context.area.type == 'DOPESHEET_EDITOR':
            self._handle = bpy.types.SpaceDopeSheetEditor.draw_handler_add(
                draw_set_range_callback, (self, context), 'WINDOW', 'POST_PIXEL')

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}


def draw_set_previewrange_callback(op, context):
    draw_handle_selection(op.hover_frame, (0.631373, 0.301961, 0.0, 1.0), context)


def set_preview_range_clean(handle, context):
    if context.area.type == 'GRAPH_EDITOR':
        bpy.types.SpaceGraphEditor.draw_handler_remove(handle, 'WINDOW')
    elif context.area.type == 'DOPESHEET_EDITOR':
        bpy.types.SpaceDopeSheetEditor.draw_handler_remove(handle, 'WINDOW')


class BRD_FRAMERANGE_OT_set_preview_range(bpy.types.Operator):
    """Set the preview range"""
    bl_idname = "brd_framerange.set_preview_range"
    bl_label = "Set the scene's preview range"
    bl_options = {'REGISTER', 'UNDO'}

    mouse_x_start: bpy.props.IntProperty()

    def modal(self, context, event):
        #Get the right region, else it could run calculations on the wrong region inside the area
        context.area.tag_redraw()
        region = None
        for r in context.area.regions:
            if r.type == "WINDOW":
                region = r
                break
        scene = context.scene
        if event.type == 'MOUSEMOVE':
            x1,_ = context.region.view2d.region_to_view(self.mouse_x_start - region.x, 0)
            x2,_ = context.region.view2d.region_to_view(event.mouse_x - region.x, 0)
            start = round(x1)
            self.hover_frame = end = round(x2)
            scene.frame_preview_start = start if start < end else end
            scene.frame_preview_end = end if start < end else start
            return {'RUNNING_MODAL'}

        if event.type in {'ENTER', 'LEFTMOUSE'}:
            scene.use_preview_range = True if scene.frame_preview_start != scene.frame_preview_end else False
            set_preview_range_clean(self._handle, context)
            return {'FINISHED'}

        if event.type in {'ESC', 'RIGHTMOUSE'}:
            context.scene.use_preview_range = self.preview_hold
            scene.frame_preview_start = self.hold_start
            scene.frame_preview_end = self.hold_end
            set_preview_range_clean(self._handle, context)
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.preview_hold = context.scene.use_preview_range
        context.scene.use_preview_range = True
        self.mouse_x_start = event.mouse_x
        self.hold_start = context.scene.frame_preview_start
        self.hold_end = context.scene.frame_preview_end
        x,_ = context.region.view2d.region_to_view(event.mouse_x - context.region.x, 0)
        self.hover_frame = round(x)

        if context.area.type == 'GRAPH_EDITOR':
            self._handle = bpy.types.SpaceGraphEditor.draw_handler_add(
                draw_set_previewrange_callback, (self, context), 'WINDOW', 'POST_PIXEL')
        elif context.area.type == 'DOPESHEET_EDITOR':
            self._handle = bpy.types.SpaceDopeSheetEditor.draw_handler_add(
                draw_set_previewrange_callback, (self, context), 'WINDOW', 'POST_PIXEL')

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}


class BRD_FRAMERANGE_OT_timerange_clear(Operator):
    """Clear the scene's time selection"""
    bl_idname = "brd_framerange.timerange_clear"
    bl_label = "Clear Selected Time Range"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        bfr = context.scene.brd_frame_range

        for h in bfr.range.handles:
            h.select = False
            h.frame = 0

        bfr.update_range()
        
        tag_redraw_animation_spaces(context)

        return {'FINISHED'}


class BRD_FRAMERANGE_OT_grab_timerange_handles(bpy.types.Operator):
    """Move framerange handles"""
    bl_idname = "brd_framerange.grab_timerange_handles"
    bl_label = "Move the handles of the scene's time range"
    bl_options = {'REGISTER', 'UNDO', 'GRAB_CURSOR', 'BLOCKING'}

    mouse_x_start: IntProperty()

    def modal(self, context, event):
        context.area.tag_redraw()
        
        if event.type == 'MOUSEMOVE':
            x1,_ = context.region.view2d.region_to_view(self.mouse_x_start, 0)
            x2,_ = context.region.view2d.region_to_view(event.mouse_x, 0)
            offset = round(x2 - x1)
            for i in range(0, len(self.selected_handles)):
                handle = self.selected_handles[i]
                handle.frame = self.selected_holds[i] + offset
            context.scene.brd_frame_range.update_range()
            return {'RUNNING_MODAL'}

        if event.type in {'ENTER', 'LEFTMOUSE'}:
            return {'FINISHED'}

        if event.type in {'ESC', 'RIGHTMOUSE'}:
            sfr = context.scene.brd_frame_range
            for i in range(0, len(sfr.range.handles)):
                handle = sfr.range.handles[i]
                handle.frame = self.handle_frame_holds[i]
            sfr.update_range()
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.mouse_x_start = event.mouse_x
        self.selected_handles = []
        self.selected_holds = []
        self.handle_frame_holds = []

        for handle in context.scene.brd_frame_range.range.handles:
            if handle.select:
                self.selected_handles.append(handle)
                self.selected_holds.append(handle.frame)
            self.handle_frame_holds.append(handle.frame)

        if len(self.selected_handles) == 0:
            return {'CANCELLED'}

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}


def pick_inside_two_handles(mouse_x, handle_1, handle_2, handle_1_pixel, handle_2_pixel):
    distance = handle_2_pixel - handle_1_pixel
    multiplier = 0.25
    reach_inside = distance * multiplier
    # reach outside_left
    if handle_1_pixel <= mouse_x <= (handle_1_pixel + reach_inside):
        return [handle_1]
    elif (handle_2_pixel - reach_inside) <= mouse_x <= handle_2_pixel:
        return [handle_2]
    else:
        return [handle_1, handle_2]
    return []


# Returns an array with TimeRange.handles depending on
# where the mouse lies within the timeline
def find_closest_time_range_handles(mouse_x, range, range_frames_region_pixels):
    # Outside the left falloff
    if range_frames_region_pixels[0] - 40 < mouse_x < range_frames_region_pixels[0]:
        return [range.handles[0]]

    # Determine the handles when mouse is between the handles on the left
    if range.handles[0].frame == range.handles[1].frame:
        if range_frames_region_pixels[0] - 10 <= mouse_x <= range_frames_region_pixels[1] + 10:
            return [range.handles[0], range.handles[1]]
    elif range_frames_region_pixels[0] <= mouse_x <= range_frames_region_pixels[1]:
        if range.handles[0].frame < range.handles[1].frame:
            return pick_inside_two_handles(
                mouse_x, 
                range.handles[0], range.handles[1], 
                range_frames_region_pixels[0], range_frames_region_pixels[1])

    # Determine the handles when mouse is between the handles in the middle
    if range.handles[1].frame == range.handles[2].frame:
        if range_frames_region_pixels[1] - 10 <= mouse_x <= range_frames_region_pixels[2] + 10:
            return [range.handles[1], range.handles[2]]
    elif range_frames_region_pixels[1] <= mouse_x <= range_frames_region_pixels[2]:
        if range.handles[1].frame < range.handles[2].frame:
            return pick_inside_two_handles(
                mouse_x, 
                range.handles[1], range.handles[2], 
                range_frames_region_pixels[1], range_frames_region_pixels[2])

    # Determine the handles when mouse is between the handles on the right
    if range.handles[2].frame == range.handles[3].frame:
        if range_frames_region_pixels[2] - 10 <= mouse_x <= range_frames_region_pixels[3] + 10:
            return [range.handles[2], range.handles[3]]
    elif range_frames_region_pixels[2] <= mouse_x <= range_frames_region_pixels[3]:
        if range.handles[2].frame < range.handles[3].frame:
            return pick_inside_two_handles(
                mouse_x, 
                range.handles[2], range.handles[3], 
                range_frames_region_pixels[2], range_frames_region_pixels[3])

    # Outside the right falloff
    if range_frames_region_pixels[3] < mouse_x < range_frames_region_pixels[3] + 40:
        return [range.handles[3]]

    return []


class BRD_FRAMERANGE_OT_select_framerange_handle(bpy.types.Operator):
    """Select framerange handle"""
    bl_idname = "brd_framerange.select_framerange_handle"
    bl_label = "Selects a handle for the framerange"
    bl_options = {'REGISTER', 'UNDO'}

    action : EnumProperty(
        name = "Action",
        description = "What action to perform when using the select handles op",
        items = [
            ('SET', 'Set', 'Select one handle from the framerange', '', 0),
            ('EXTEND', 'Extend', 'Extends the framrange selection', '', 1),
        ],
        default='SET'
    )

    def invoke(self, context, event):
        region = None
        for r in context.area.regions:
            if r.type == "WINDOW":
                region = r
                break

        handle_pixels = []
        sfr = get_framerange(context)

        if sfr.range.handles[0].frame == sfr.range.handles[3].frame:
            return {'CANCELLED'}

        for i in range(len(sfr.range.handles)):
            x,_ = region.view2d.view_to_region(sfr.range.handles[i].frame, 0, clip=False)
            handle_pixels.append(x)

        if self.action == "EXTEND":
            ## Determine which handles to select ##
            tmp_handles = []
            is_deselected = False
            for handle in find_closest_time_range_handles(
                event.mouse_x - region.x, sfr.range, handle_pixels):
                if not handle.select:
                    is_deselected = True
                tmp_handles.append(i)

            for i in tmp_handles:
                if is_deselected:
                    sfr.range.handles[i].select = True
                else:
                    sfr.range.handles[i].select = False

        elif self.action == "SET":
            for i in range(len(sfr.range.handles)):
                sfr.range.handles[i].select = False
            ## Determine which handles to select ##
            for handle in find_closest_time_range_handles(
                event.mouse_x - region.x, sfr.range, handle_pixels):
                handle.select = True

        tag_redraw_animation_spaces(context)

        # Using passthrough here fixes click and drag events, for example
        return {"FINISHED", "PASS_THROUGH"}


class BRD_FRAMERANGE_OT_select_all_handles(bpy.types.Operator):
    """Select/Deselect/Invert selection of scene framerange handles"""
    bl_idname = "brd_framerange.select_all_handles"
    bl_label = "Select/Deselect/Invert selection of scene framerange handles"
    bl_options = {'REGISTER', 'UNDO'}

    action : EnumProperty(
        name = "Action",
        description = "What action to perform when using the select handles op",
        items = [
            ('SELECT', 'Select', 'Select all handles', '', 0),
            ('DESELECT', 'Deselect', 'Deselect all handles', '', 1),
            ('INVERT', 'Invert', 'Invert handle select', '', 2),
        ]
    )

    def invoke(self, context, event):
        if self.action == 'SELECT':
            for handle in context.scene.brd_frame_range.range.handles:
                handle.select = True
                
        elif self.action == 'DESELECT':
            for handle in context.scene.brd_frame_range.range.handles:
                handle.select = False
            # return {'FINISHED'}

        elif self.action == 'INVERT':
            for handle in context.scene.brd_frame_range.range.handles:
                handle.select = not handle.select
            # return {'FINISHED'}

        tag_redraw_animation_spaces(context)

        # HACK: ?
        bpy.ops.ed.undo_push()
        return {'FINISHED'}


class BRD_FRAMERANGE_OT_grow_falloff_handles(bpy.types.Operator):
    """Grow Falloff Handles"""
    bl_idname = "brd_framerange.grow_falloff_handles"
    bl_label = "Expands/Contracts the outer handles of the scene's time range"
    bl_options = {'REGISTER', 'UNDO', 'UNDO_GROUPED', 'GRAB_CURSOR', 'BLOCKING'}

    action : EnumProperty(
        name = "Action",
        description = "What action to perform when using the select handles op",
        items = [
            ('SCALE', 'Scale', 'Use modal to grow based on mouse movement', '', 0),
            ('SCROLL', 'Scroll', 'Expand/Shrink falloffs using mousewheel ticks', '', 1),
        ],
        default='SCALE'
    )
    direction : IntProperty(
        default=1
    )

    def modal(self, context, event):
        if event.type == 'MOUSEMOVE':
            bfr = context.scene.brd_frame_range
            x1,_ = context.region.view2d.region_to_view(self.mouse_x_start, 0)
            x2,_ = context.region.view2d.region_to_view(event.mouse_x, 0)
            offset = round(x2 - x1)

            bfr.range.handles[0].frame = self.left_hold - offset
            bfr.range.handles[3].frame = self.right_hold + offset
            bfr.update_range()

            context.area.tag_redraw()
            return {'RUNNING_MODAL'}

        if event.type in {'ENTER', 'LEFTMOUSE'}:
            return {'FINISHED'}

        if event.type in {'ESC', 'RIGHTMOUSE'}:
            bfr = context.scene.brd_frame_range
            bfr.range.handles[0].frame = self.left_hold
            bfr.range.handles[3].frame = self.right_hold
            bfr.update_range()

            context.area.tag_redraw()
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        bfr = context.scene.brd_frame_range

        region = context.region

        if bfr.range.start == bfr.range.end:
            hover_frame,_ = region.view2d.region_to_view(event.mouse_x - region.x, 0)
            hover_frame = round(hover_frame)

            for h in bfr.range.handles:
                h.frame = hover_frame

        if self.action == 'SCROLL':
            bfr.range.handles[0].frame -= self.direction
            bfr.range.handles[3].frame += self.direction
            bfr.update_range()

            context.area.tag_redraw()
            return {'FINISHED'}

        elif self.action == 'SCALE':
            self.mouse_x_start = event.mouse_x
            self.handle_frame_holds = []

            for handle in context.scene.brd_frame_range.range.handles:
                self.handle_frame_holds.append(handle.frame)

            self.left_hold = bfr.range.handles[0].frame
            self.right_hold = bfr.range.handles[3].frame

            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}

        self.report({'WARNING'}, "????")
        return {'CANCELLED'}


class BRD_FRAMERANGE_OT_snap_timerange_handles(bpy.types.Operator):
    """Snap the scenes framerange handles towards a direction"""
    bl_idname = "brd_framerange.snap_timerange_handles"
    bl_label = "Snap Scene's time range handles"
    bl_options = {'REGISTER', 'UNDO'}

    direction : EnumProperty(
        name = "Direction",
        description = "Which direction to snap to",
        items = [
            ('LEFT', 'Left', 'Snap to first deselected towards the left', '', 0),
            ('RIGHT', 'Right', 'Snap to first deselected towards the right', '', 1),
            ('START', 'Start', 'Snap to scene start frame', '', 2),
            ('END', 'End', 'Snap to scene end frame', '', 3),
        ]
    )

    def invoke(self, context, event):
        brd_frame_range = context.scene.brd_frame_range
        selected_handles = []
        handles_to_select = []
        handle_len = len(brd_frame_range.range.handles)
        for i in range(0, handle_len):
            handle = None
            if self.direction == 'LEFT':
                handle = brd_frame_range.range.handles[handle_len-1 - i]
            elif self.direction == 'RIGHT':
                handle = brd_frame_range.range.handles[i]

            if handle.select:
                selected_handles.append(handle)
            elif len(selected_handles) > 0:
                for h in selected_handles:
                    h.frame = handle.frame
                handles_to_select.append(handle)
                selected_handles.clear()

        # If there are still selected handles, then snap to the scene start/end
        for h in selected_handles:
            h.frame = context.scene.frame_start if self.direction == 'LEFT' else context.scene.frame_end

        for h in handles_to_select:
            h.select = True

        brd_frame_range.update_range()
        context.area.tag_redraw()
        return {'FINISHED'}


class BRD_FRAMERANGE_OT_scale_timerange_handles(bpy.types.Operator):
    """Scale framerange handles from the pivot point"""
    bl_idname = "brd_framerange.scale_timerange_handles"
    bl_label = "Scale Scene TimeRange Handles"
    bl_options = {'REGISTER', 'UNDO', 'GRAB_CURSOR', 'BLOCKING'}

    def modal(self, context, event):
        context.area.tag_redraw()
        
        if event.type == 'MOUSEMOVE':
            x1,_ = context.region.view2d.region_to_view(self.mouse_x_start - context.region.x, 0)
            x2,_ = context.region.view2d.region_to_view(event.mouse_x - context.region.x, 0)
            scale = normalize(x2, self.center, x1)
            for i in range(0, len(self.selected_handles)):
                handle = self.selected_handles[i]
                offset = (self.selected_holds[i] - self.center) * (scale if scale > 0 else 0)
                handle.frame = round(offset + self.center)
            context.scene.brd_frame_range.update_range()
            context.area.tag_redraw()
            return {'RUNNING_MODAL'}

        if event.type in {'ENTER', 'LEFTMOUSE'}:
            context.scene.brd_frame_range.update_range()
            return {'FINISHED'}

        if event.type in {'ESC', 'RIGHTMOUSE'}:
            sfr = context.scene.brd_frame_range
            for i in range(0, len(sfr.range.handles)):
                handle = sfr.range.handles[i]
                handle.frame = self.handle_frame_holds[i]
            sfr.update_range()
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.mouse_x_start = event.mouse_x
        self.selected_handles = []
        self.selected_holds = []
        self.handle_frame_holds = []

        self.region = None
        for r in context.area.regions:
            if r.type == "WINDOW":
                self.region = r
                break

        for handle in context.scene.brd_frame_range.range.handles:
            if handle.select:
                self.selected_handles.append(handle)
                self.selected_holds.append(handle.frame)

            self.handle_frame_holds.append(handle.frame)

        if len(self.selected_handles) == 0:
            return {'CANCELLED'}


        space_data = context.space_data
        if context.area.type == 'DOPESHEET_EDITOR' or space_data.pivot_point == 'BOUNDING_BOX_CENTER':
            minimum = min(handle.frame for handle in self.selected_handles)
            maximum = max(handle.frame for handle in self.selected_handles)
            if minimum == maximum:
                return {'CANCELLED'}
            self.center = (minimum + maximum) / 2
        elif space_data.pivot_point == 'CURSOR':
            self.center = context.scene.frame_current

        # return {'FINISHED'}
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}