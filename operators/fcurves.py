# Procedural Fcurve Editor, Adds tools to make modifying fcurves easier.
# Copyright (C) 2020 bird_d <relay198@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import bpy
from bpy.props import (IntProperty, 
                       FloatProperty, 
                       StringProperty, 
                       BoolProperty, 
                       EnumProperty)
from ..utils import *
import numpy as np
import logging
import time
import random


class PFC_Filter(bpy.types.Operator):
    """TEST"""
    bl_idname = "pfc.unnamed_filter"
    bl_label = "Unnamed Filter!"
    bl_space_type = 'GRAPH_EDITOR'
    bl_options = {'REGISTER', 'UNDO', 'GRAB_CURSOR', 'BLOCKING'}

    log = None
    bfr = None
    # The FCurves we want to modify
    fcurves = []
    fcurve_index = 0
    # Held data from the FCurves from before we modified them, so we can 
    # restore on cancel
    hcurves = []
    #Evaluated curve points
    ecurves = []
    # Data we multiply the strength and bfr.get_frame_influence against 
    # before placing on an FCurve
    filter_data = []

    mouse_x_start = 0
    mouse_y_start = 0
    mouse_relative_x = 0
    mouse_relative_y = 0
    # From the edge of the screen by default, based on what's chosen by 
    # self.vertical_drag
    distance = 0
    deselect = False
    
    # Properties that need to be reimplemented per operator.
    # no way I (bird_d) can see around this
    clamped = None
    strength = None
    iteration = None
    
    # Overridable options
    vertical_drag = False
    use_clamp = False
    use_iteration = False


    def __init__(self):
        self.log = logging.getLogger("%s.%s" % (__name__, self.bl_idname))

    """Overwritable methods

        When creating a new filter, these are methods you're allowed to
        modify if you want to create a different behaviour
    """
    def start(self, context):
        pass


    def execute(self, context):
        if self.clamped == True:
            self.strength = clamp(self.strength, 0, 1)
        self.fcurves = get_active_fcurves(context)

        self.__build_data(context)
        self.start(context)

        i = 1
        while len(self.filter_data[0]) \
                <= self.iteration if self.use_iteration else 0:
            self.__add_iteration(context, i)
            i += 1

        self.__update(context)
        self.__clean(context)
        return {'FINISHED'}


    def modal(self, context, event):
        for area in context.screen.areas:
            if area.type in {'GRAPH_EDITOR', 'VIEW_3D'}:
                area.tag_redraw()
        ##TODO: add better tweaking via shift that uses area? current 
        # one should work for now tho.
        if event.type == 'MOUSEMOVE' or event.type == "LEFT_SHIFT":
            self.strength = self.strength_function(context, event)
            self.__update(context)
        if event.type == 'C' and self.use_clamp:
            if event.value == 'PRESS':
                self.clamped = not self.clamped
                self.__update(context)
        #TODO: Maybe base iteration on if you go above 1 strength while 
        # clamped?
        if self.use_iteration:
            if event.type == 'WHEELUPMOUSE':
                self.iteration += 1
                self.__add_iteration(context, self.iteration)
                self.__update(context)
            if event.type == 'WHEELDOWNMOUSE':
                self.iteration -= 1 if self.iteration > 0 else 0
                self.__update(context)

        if event.type == 'LEFTMOUSE':
            self.__clean(context)
            return {'FINISHED'}
        if event.type in {'RIGHTMOUSE', 'ESC'}:
            self.__clean(context, cancelled=True)
            return {'CANCELLED'}
        header_text = "Distance: %.3f, Strength: %.3f" % (self.distance, 
                                                          self.strength)
        if self.use_clamp:
            header_text += (", [C]lamp: %s" % ("ON" \
                                               if self.clamped else \
                                               "OFF"))
        if self.use_iteration:
            header_text += (", Iteration: %s" % self.iteration)
        context.area.header_text_set(text=header_text)
        return {'RUNNING_MODAL'}


    def clean(self, context):
        pass


    def filter_function(self, context, fcurve, frame_index, iteration=0):
        return 0


    # If not overriden, then just return the filter_data
    def adjust_function(
            self, context, fcurve, frame_index, evaluated_data, filter_data):
        return filter_data


    def strength_function(self, context, event):
        a = event.mouse_y if self.vertical_drag else event.mouse_x
        b = self.mouse_y_start if self.vertical_drag else self.mouse_x_start
        return clamp((a - b) / (100, 1000)[event.shift]) \
               if self.clamped else \
               (a - b) / (100, 1000)[event.shift]


    def distance_function(self, context, event):
        #HACK: This should prob also go by x region too if a variable is true
        region = None
        for r in context.area.regions:
            if r.type == "WINDOW":
                region = r
                break
        _,a = region.view2d.region_to_view(0, region.height)
        _,b = region.view2d.region_to_view(0, region.y)            
        return abs(a - b)


    @classmethod
    def poll(cls, context):
        return True


    def invoke(self, context, event):
        if bpy.context.area.type != 'GRAPH_EDITOR':
            self.report({'WARNING'}, 
                        "Somehow didn't run this in graph editor area")
            return {'CANCELLED'}

        self.bfr = context.scene.brd_frame_range
        if self.bfr.range.start == self.bfr.range.end:
            self.report({'WARNING'}, "No area selected to modify.")
            return {'CANCELLED'}

        self.fcurves = get_active_fcurves(context)
        if len(self.fcurves) == 0:
            self.report({'WARNING'}, "Must have Fcurve channel(s) selected.")
            return {'CANCELLED'}

        self.deselect = get_preferences().deselect_new_keyframes
        self.distance = self.distance_function(context, event)
        self.mouse_x_start, self.mouse_y_start = (event.mouse_x, event.mouse_y)
        if self.use_iteration:
            self.iteration = 0
        self.strength = 0

        self.__build_data(context)
        self.start(context)
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    """Private methods
    
        Common methods for filter functions that shouldn't be called directly
        or overwritten by a child class
    """
    def __update(self, context):
        self.fcurve_index = 0
        self.influence_array = []
        frame_range = range(self.bfr.range.start, self.bfr.range.end+1)

        for i in frame_range:
            self.influence_array.append(self.strength 
                                        * self.bfr.get_frame_influence(i))

        for i in range(len(self.fcurves)):
            self.fcurve_index = i
            fc = self.fcurves[i]
            # keyframes = self.keyframes[i]
            keyframes = {}
            for key in fc.keyframe_points:
                keyframes[key.co[0]] = key
            filter_data = self.filter_data[i][self.iteration \
                                              if self.use_iteration else \
                                              0]
            efc = self.ecurves[i]

            adjusted_data = self.adjust_function(context, 
                                                 fc, 
                                                 0, 
                                                 efc, 
                                                 filter_data)
            scaled_data = self.influence_array * (adjusted_data - efc) + efc

            for frame, data in zip(frame_range, scaled_data):
                if frame in keyframes:
                    key = keyframes[frame]
                    key.co[1] = data
                    continue
                insert_keyframe_point(fcurve=fc,
                                      frame=frame,
                                      value=data,
                                      options={"FAST"},
                                      keyframe_type="JITTER",
                                      deselect=self.deselect)
            fc.update()

        del self.influence_array


    def __add_iteration(self, context, iteration):
        if iteration >= len(self.filter_data[0]):
            self.fcurve_index = 0
            for i in range(0, len(self.fcurves)):
                self.fcurve_index = i
                filter_data = self.filter_data[i]
                tmp_data_arr = []
                for frame_index in range(self.bfr.range.start, 
                                         self.bfr.range.end+1):
                    # If our frame == start or end frame, we just use 
                    # the current value of the Fcurve on that frame
                    if (frame_index == self.bfr.range.start 
                            or frame_index == self.bfr.range.end):
                        prev_data = filter_data[iteration-1]
                        tmp_data_arr.append(
                            prev_data[frame_index-self.bfr.range.start])
                        continue

                    tmp_data_arr.append(self.filter_function(context, 
                                                             self.fcurves[i], 
                                                             frame_index, 
                                                             iteration))
                # filter_data[fcurve]|[iteration][frame_data]
                self.filter_data[self.fcurve_index].append(
                    np.array(tmp_data_arr))


    def __build_data(self, context):
        frame_range = range(self.bfr.range.start, self.bfr.range.end+1)
        frame_count = len(frame_range)
        fcurve_count = len(self.fcurves)

        # Allocate numpy array memory
        self.ecurves = np.zeros((fcurve_count, frame_count), dtype=float)

        for fcurve_index in range(fcurve_count):
            fc = self.fcurves[fcurve_index]
            # Disable FCurve modifiers so when we evaluate curves, we 
            # don't use that data
            modifier_holds = []
            for modifier in fc.modifiers:
                modifier_holds.append((modifier, modifier.mute))
                modifier.mute = True
            # Fill data, so we can return to it on cancel if we need to
            kp_arr = {}
            for kp in fc.keyframe_points:
                if self.bfr.range.start <= kp.co[0] <= self.bfr.range.end:
                    kp_arr[kp.co[0]] \
                        = {"value" : kp.co[1], 
                           "handle_left" : kp.handle_left,
                           "handle_left_type" : kp.handle_left_type,
                           "handle_right" : kp.handle_right,
                           "handle_right_type" : kp.handle_right_type,
                           "interpolation" : kp.interpolation}
            self.hcurves.append(kp_arr)

            tmp_data_arr = np.zeros(frame_count, dtype=float)
            tmp_eval_arr = []
            for i in range(self.bfr.range.start, self.bfr.range.end+1):
                tmp_eval_arr.append(fc.evaluate(i))
                # If our frame == start or end frame, we just use the 
                # current value of the Fcurve on that frame
                if i == self.bfr.range.start or i == self.bfr.range.end:
                    tmp_data_arr[i-self.bfr.range.start] = fc.evaluate(i)
                else:
                    tmp_data_arr[i-self.bfr.range.start] \
                        = self.filter_function(context, fc, i)

            # filter_data|[fcurve number][iteration][frame_data]
            self.filter_data.append([tmp_data_arr])
            self.ecurves[fcurve_index] = tmp_eval_arr

            # Re-enable modifiers
            for modifier, mute in modifier_holds:
                modifier.mute = mute


    def __clean(self, context, cancelled=False):
        if cancelled:
            # Restore FCurves to how they were
            for i in range(len(self.fcurves)):
                fc = self.fcurves[i]
                bfr = bpy.context.scene.brd_frame_range

                old_keyframes = self.hcurves[i]

                index = len(fc.keyframe_points)-1
                while index > 0:
                    key = fc.keyframe_points[index]
                    index -= 1
                    if not bfr.range.start <= key.co[0] <= bfr.range.end:
                        continue
                    if key.co[0] in old_keyframes:
                        old_key = old_keyframes[key.co[0]]
                        key.co[1] = old_key["value"]
                        key.handle_left = old_key["handle_left"]
                        key.handle_left_type = old_key["handle_left_type"]
                        key.handle_right = old_key["handle_right"]
                        key.handle_right_type = old_key["handle_right_type"]
                        key.interpolation = old_key["interpolation"]
                        continue

                    fc.keyframe_points.remove(key, fast=True)

                fc.update()

        # Free array memory
        self.fcurves.clear()
        self.hcurves.clear()
        del self.ecurves
        self.filter_data.clear()
        # Fix UI things we modified
        context.area.header_text_set(text=None)
        context.window.cursor_modal_restore()
        self.clean(context)


class GRAPH_OT_pfc_filter_noise(PFC_Filter):
    """Add noise to an FCurve"""
    bl_idname = "graph.pfc_filter_noise"
    bl_label = "Noise FCurve Filter"

    strength : bpy.props.FloatProperty(name="Strength")

    vertical_drag = True


    def filter_function(self, context, fcurve, frame_index):
        return random.random()-0.5


    def adjust_function(
            self, context, fcurve, frame_index, 
            evaluated_data, filter_data):
        # Use evaluated fcurve data to offset the noise, so it follows curve
        # and doesn't start from zero on every keyframe
        return evaluated_data + filter_data * self.distance


class GRAPH_OT_pfc_filter_wma_smooth(PFC_Filter):
    """Apply Weighted Moving Average Smoothing to an FCurve"""
    bl_idname = "graph.pfc_filter_wma_smooth"
    bl_label = "WMA Smoothing FCurve Filter"
    
    clamped : BoolProperty(name="Clamped", default=True)
    strength : FloatProperty(name="Strength")
    iteration : IntProperty(name="Iteration", min=0, max=50)

    use_clamp = True
    use_iteration = True


    def filter_function(self, context, fcurve, frame_index, iteration=0):
        weights = [0.1, 0.25, 0.5, 0.25, 0.1]

        if iteration == 0 or len(self.filter_data) == 0:
            return ((fcurve.evaluate(frame_index-2) * weights[0]) \
                        + (fcurve.evaluate(frame_index-1) * weights[1]) \
                        + (fcurve.evaluate(frame_index) * weights[2]) \
                        + (fcurve.evaluate(frame_index+1) * weights[3]) \
                        + (fcurve.evaluate(frame_index+2) * weights[4])) \
                    / sum(weights)

        # How to calculate when we're iterating 
        previous_filter_data = self.filter_data[self.fcurve_index][iteration-1]
        i = frame_index - self.bfr.range.start
        a = fcurve.evaluate(frame_index-2) \
            if i-2 < 2 else \
            previous_filter_data[i-2]
        b = fcurve.evaluate(frame_index-1) \
            if i-1 < 1 else \
            previous_filter_data[i-1]
        c = previous_filter_data[i]
        d = fcurve.evaluate(frame_index+1) \
            if i+1 >= len(previous_filter_data) else \
            previous_filter_data[i+1]
        e = fcurve.evaluate(frame_index+2) \
            if i+2 >= len(previous_filter_data) else \
            previous_filter_data[i+2]


        return ((a * weights[0]) \
                    + (b * weights[1]) \
                    + (c * weights[2]) \
                    + (d * weights[3]) \
                    + (e * weights[4])) \
                / sum(weights)


class GRAPH_OT_pfc_filter_ramp(PFC_Filter):
    """Modifies FCurve to go from selection start to end linearly."""
    bl_idname = "graph.pfc_filter_ramp"
    bl_label = "Ramp FCurve Filter"
    
    clamped : BoolProperty(name="Clamped", default=True)
    strength : FloatProperty(name="Strength")

    use_clamp = True
    clamped = True


    def filter_function(self, context, fcurve, frame_index):
        a, b, s = (None, None, None)
        if frame_index < self.bfr.range.handles[1].frame:
            a = fcurve.evaluate(self.bfr.range.handles[0].frame)
            b = fcurve.evaluate(self.bfr.range.handles[1].frame)
            s = normalize(frame_index,
                          self.bfr.range.handles[0].frame,
                          self.bfr.range.handles[1].frame)
        elif frame_index > self.bfr.range.handles[2].frame:
            a = fcurve.evaluate(self.bfr.range.handles[2].frame)
            b = fcurve.evaluate(self.bfr.range.handles[3].frame)
            s = normalize(frame_index,
                          self.bfr.range.handles[2].frame,
                          self.bfr.range.handles[3].frame)
        else:
            a = fcurve.evaluate(self.bfr.range.handles[1].frame)
            b = fcurve.evaluate(self.bfr.range.handles[2].frame)
            s = normalize(frame_index,
                          self.bfr.range.handles[1].frame,
                          self.bfr.range.handles[2].frame)
        return scale(s,a,b)


class GRAPH_OT_pfc_filter_cursor(PFC_Filter):
    #Inherited options
    """Modifies FCurve to go towards a value, usually the cursor"""
    bl_idname = "graph.pfc_filter_cursor"
    bl_label = "Cursor FCurve Filter"

    clamped : BoolProperty(name="Clamped", default=True)
    strength : bpy.props.FloatProperty(name="Strength")

    use_clamp = True
    clamped = True


    def filter_function(self, context, fcurve, frame_index):
        for space in bpy.context.area.spaces:
            if space.type == 'GRAPH_EDITOR':
                return space.cursor_position_y


class GRAPH_OT_pfc_filter_playhead(PFC_Filter):
    #Inherited variables
    """Modifies FCurve towards the value under the playhead"""
    bl_idname = "graph.pfc_filter_playhead"
    bl_label = "Cursor FCurve Filter"

    clamped : BoolProperty(name="Clamped", default=True)
    strength : bpy.props.FloatProperty(name="Strength")

    use_clamp = True
    clamped = True


    #TODO: Make choosable between all fcurves or active fcurve
    def filter_function(self, context, fcurve, frame_index):
        return context.active_editable_fcurve.evaluate(
            context.scene.frame_current)


    @classmethod
    def poll(cls, context):
        if context.active_editable_fcurve == None:
            return False
        return True


class GRAPH_OT_pfc_filter_zero(PFC_Filter):
    #Inherited variables
    """Modifies FCurve to go towards zero."""
    bl_idname = "graph.pfc_filter_zero"
    bl_label = "Cursor FCurve Filter"
    clamped : BoolProperty(name="Clamped", default=True)
    strength : bpy.props.FloatProperty(name="Strength")

    use_clamp = True
    clamped = True


    def filter_function(self, context, fcurve, frame_index):
        return 0


class GRAPH_OT_pfc_filter_limit(PFC_Filter):
    #Inherited variables
    """Forces points above or below the cursor to go to the cursor"""
    bl_idname = "graph.pfc_filter_limit"
    bl_label = "Limit FCurve Filter"

    clamped : BoolProperty(name="Clamped", default=True)
    strength : bpy.props.FloatProperty(name="Strength")

    use_clamp = True
    clamped = True
    #Non-inherited variables
    limit_type : EnumProperty(
        items=[
            ('FLOOR', 'Floor', '', 'EXPORT', 0),
            ('CEILING', 'Ceiling', '', 'IMPORT', 1),
        ],
        name="Limit type",
        default='FLOOR'
    )
    type : StringProperty(default='')


    def filter_function(self, context, fcurve, frame_index):
        evaluated_frame = fcurve.evaluate(frame_index)
        for space in context.area.spaces:
            if space.type == 'GRAPH_EDITOR':
                self.limit_type = self.type
                if self.type == 'FLOOR':
                    return max(evaluated_frame, space.cursor_position_y)
                if self.type == 'CEILING':
                    return min(evaluated_frame, space.cursor_position_y)


##TODO: Use this as a modal for the strength/phase offset as well?
class GRAPH_OT_procedural_range_to_modifier(bpy.types.Operator):
    """Adds or changes an Fcurve modifier with the frame range and falloff range of the selected procedural range"""
    bl_idname = "graph.procedural_range_to_modifier"
    bl_label = "Procedural Range to Modifier"
    bl_options = {'REGISTER', 'UNDO'}
    log = logging.getLogger("%s.%s" % (__name__, bl_idname))
    #TODO: Probably use an Enum for this?
    modifier : StringProperty(name="")
    fcurves = []


    def invoke(self, context, event):
        self.fcurves = get_active_fcurves(context)
        if handle_common_errors(self) == False:
            return {'CANCELLED'}

        mods = []
        if self.modifier == 'ACTIVE':
            mods.append(bpy.context.active_editable_fcurve.modifiers.active)
        else:
            for fcurve in self.fcurves:
                if self.modifier == 'GENERATOR':
                    mods.append(fcurve.modifiers.new(type='GENERATOR'))
                elif self.modifier == 'FNGENERATOR':
                    mods.append(fcurve.modifiers.new(type='FNGENERATOR'))
                elif self.modifier == 'ENVELOPE':
                    mods.append(fcurve.modifiers.new(type='ENVELOPE'))
                elif self.modifier == 'CYCLES':
                    mods.append(fcurve.modifiers.new(type='CYCLES'))
                elif self.modifier == 'NOISE':
                    mods.append(fcurve.modifiers.new(type='NOISE'))
                elif self.modifier == 'LIMITS':
                    mods.append(fcurve.modifiers.new(type='LIMITS'))
                elif self.modifier == 'STEPPED':
                    mods.append(fcurve.modifiers.new(type='STEPPED'))
        
        bfr = context.scene.brd_frame_range
        for mod in mods:
            mod.use_restricted_range = True
            mod.frame_start = bfr.range.start
            mod.frame_end = bfr.range.end
            mod.blend_in = (bfr.range.handles[1].frame
                            - bfr.range.handles[0].frame)
            mod.blend_out = (bfr.range.handles[3].frame 
                             - bfr.range.handles[2].frame)
            
        self.fcurves.clear()
        return {'FINISHED'}